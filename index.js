const http = require('http');
const { listenerCount } = require('process');

const PORT = 3000;

const server = http.createServer((request, response) => {

    if (request.url == '/login') {
        response.writeHead(200, { 'Content-Type': 'text/pain' });
        response.end('Welcome to the login page.');
    } else {
        response.writeHead(404, { 'Content-Type': 'text/pain' });
        response.end(`I'm sorry the page you are looking for cannot be found.`);
    }
})

server.listen(PORT);

console.log(`Server is runnig at localhost:${PORT}`)